---
author: admin
date: 2018-11-10 00:28:38+00:00
draft: false
title: Tutoriels Migale
type: page
url: /about/
---

We provide tutorials and associated documentation for using the resources available on the Migale platform. You will find help to use our infrastructure (migale server, computing cluster, Galaxy, rstudio) and to use tools that we provide you with.
