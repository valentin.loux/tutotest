---
title: "Future Batchtools"
author: "Mahendra Mariadassou"
updatedate: 2019-02-02
thumbnail: https://inra-migale.github.io/posts/2019-03-04-future-batchtools/images/Retour_vers_le_futur_-_Logo.png
publishdate: 2015-07-23
categories: ["R", "SGE"]
tags: ["cluster", "parallel", "hugo"]
bibliography: biblio.bib
link-citations: true
type: post
draft: true
url: /2015-07-23-r-rmarkdonw.Rmd/
---



<p>Ce document explique comment configurer son compte migale pour envoyer des calculs sur le cluster via SGE directement depuis R en passant batchtools et future et en ne modifiant que très peu son code R <span class="citation">Escudié et al. (<a href="#ref-frogs">2017</a>)</span>.</p>
<hr />
<div id="préambule-sur-future" class="section level1">
<h1>Préambule sur <code>future</code></h1>
<p>Le but du package <a href="https://cran.r-project.org/web/packages/future/"><code>future</code></a> est de fournir une interface simple et uniforme pour évaluer des expressions R de façon asynchrone en se servant des différents paradigmes de calculs disponible pour l’utilisateur.</p>
<p>Le concept est assez simple:</p>
<ul>
<li>on définit un <em>future</em> <code>a</code>, une abstraction d’une valeur, qui à un temps donné peut-être résolue ou non (la façon exacte d’évaluer l’expression dépend du paradigme de calcul utilisé).</li>
<li>si une expression fait appel à <code>a</code> alors qu’il n’est pas encore résolu, l’évaluation de l’expression est suspendue jusqu’à ce que <code>a</code> soit résolu.</li>
</ul>
<p>La définition d’un future est très proche de l’opération d’assignation standard et passe par l’opérateur <code>%&lt;-%</code>.</p>
<div id="assignation-standard" class="section level2">
<h2>Assignation standard</h2>
<p>Regardons ce qui se passe pour une assignation standard avec le bout de code suivant:</p>
<pre class="r"><code>v &lt;- {
  cat(&quot;Hello World!\n&quot;)
  3.14
}</code></pre>
<pre><code>Hello World!</code></pre>
<pre class="r"><code>v</code></pre>
<pre><code>[1] 3.14</code></pre>
<p>L’expression qui permet de résoudre <code>v</code> est executée immédiatement (d’où l’apparation du message dans la console), même si la valeur de <code>v</code> n’est requise que plus tard.</p>
</div>
<div id="assignation-via-un-future" class="section level2">
<h2>Assignation via un future</h2>
<p>Si on passe par une assignation via <code>%&lt;-%</code>:</p>
<pre class="r"><code>library(future)
v %&lt;-% {
  cat(&quot;Hello World!\n&quot;)
  3.14
}
v</code></pre>
<pre><code>Hello World!</code></pre>
<pre><code>[1] 3.14</code></pre>
<p>On constate que l’expression n’est évaluée qu’au moment où la valeur <code>v</code> est appelée. On peut retenir que <code>%&lt;-%</code> crée un future implicte et est la contraction de</p>
<ul>
<li><code>f &lt;- future({ expr })</code> qui crée un future autour et de</li>
<li><code>v &lt;- value(f)</code> qui renvoie la valeur du future (et se bloque tant que <code>f</code> n’est pas résolu)</li>
</ul>
</div>
<div id="évaluation-des-futures" class="section level2">
<h2>Évaluation des futures</h2>
<p>Par défaut, <code>future</code> évalue les futures de façon séquentielle comme le ferait une session R normale. On peut néanmoins avoir recours à une évaluation asynchrone via la fonction <code>plan</code>.</p>
<p>On peut recourir à un plan séquentiel:</p>
<p>ou au contraire parallèle:</p>
</div>
<div id="vérifier-létat-de-résolution-dune-future" class="section level2">
<h2>Vérifier l’état de résolution d’une future</h2>
<p>La fonction <code>futureOf</code> permet d’extraire une future explicite d’une future implicite et de tester son état de résolution avec <code>resolved</code> sans accéder à la valeur de la future (et donc sans bloquer la console en attendant que la future soit résolue).</p>
<pre class="r"><code>a %&lt;-% {
     cat(&quot;Future &#39;a&#39; ...&quot;)
     Sys.sleep(2)
     cat(&quot;done\n&quot;)
     Sys.getpid()
}
cat(&quot;Waiting for &#39;a&#39; to be resolved ...\n&quot;)
f &lt;- futureOf(a)
count &lt;- 1
while (!resolved(f)) {
     cat(count, &quot;\n&quot;)
     Sys.sleep(0.2)
     count &lt;- count + 1
}
cat(&quot;Waiting for &#39;a&#39; to be resolved ... DONE\n&quot;)
a</code></pre>
</div>
<div id="points-dattention" class="section level2">
<h2>Points d’attention</h2>
<p>Les futures implicites ne se combinent pas très bien avec des boucles numériques…</p>
<pre class="r"><code>x &lt;- rep(NA, 10)
for (i in 1:10) {
  x[i] %&lt;-% i
}</code></pre>
<pre><code>Error: Subsetting can not be done on a &#39;logical&#39;; only to an environment: &#39;x[i]&#39;</code></pre>
<p>Il faut utiliser à la place des liste-environnements, issues du package <code>listenv</code> qui se manipulent comme des listes:</p>
<pre class="r"><code>library(listenv)
x &lt;- listenv()
for (i in 1:10) {
  x[[i]] %&lt;-% i
}
x &lt;- unlist(x)
x</code></pre>
<pre><code> [1]  1  2  3  4  5  6  7  8  9 10</code></pre>
</div>
</div>
<div id="combinaison-avec-sge" class="section level1">
<h1>Combinaison avec SGE</h1>
<p>Les possibilités offertes par <code>future</code> sont d’autant plus intéressantes dans le cadre de migale qu’on peut les combiner avec le cluster SGE au prix de quelques efforts.</p>
<div id="mise-en-place" class="section level2">
<h2>Mise en place</h2>
<p>Il faut commencer par se créer un template sge, nommé <code>.batchtools.sge.tmpl</code> dans son dossier <code>HOME</code>, dont le contenu peut être le suivant:</p>
<p>On peut par exemple modifier les paramètres du script (avec la syntaxe usuel de <code>qsub</code>) pour recevoir des mails lorsqu’un job est lancé ou se termine. Les valeurs <code>resources$attributs</code> sont spéciales et correspondent qui pourront être paramétrés depuis <code>R</code> lors de la mise en place du paradigme de calcul.</p>
<p>il faut ensuite se connecter sur migale et lancer une session R (via rstudio ou en ligne de commande)</p>
<pre class="bash"><code>ssh migale.jouty.inra.fr
rstudio</code></pre>
</div>
<div id="utilisation" class="section level2">
<h2>Utilisation</h2>
<p>À partir de là, il ne reste plus qu’à définir le paradigme de calcul et le reste fonctionnera comme présenté plus haut.</p>
<pre class="r"><code>library(future.batchtools) ## implémentation de backend de type SGE, SLURM, TORQUE pour future
plan(batchtools_sge, 
     workers = 10,                         ## nombre maximum d&#39;esclaves, non limité par défaut
     template = &quot;~/.batchtools.sge.tmpl&quot;,  ## template sge, inutile si nommé .batchtools.sge.tmpl et localisé
                                           ## dans HOME, car trouvé automatiquement par batchtools_sge
     resources = list(queue = &quot;short.q&quot;)   ## paramètre modifié à la volée dans le template, ici nom de la queue à utiliser
     )</code></pre>
<p>Dans la partie <strong>resources</strong> (qui doit être une liste nommée) et à condition d’avoir le template sge correspondant, on pourrait préciser d’autres options (par exemple <code>vmem = &quot;5gb&quot;</code>).</p>
<p>Le reste est strictement identique à ce qu’on ferait en local, à la différence près que la surcouche SGE induit un coût non négligeable et qu’il vaut donc mieux la réserver à des gros calculs.</p>
<pre class="r"><code>tictoc::tic()
x &lt;- listenv()
for (i in 1:10) { x[[i]] %&lt;-% i }
x &lt;- unlist(as.list(x)) ## bloqué tant que tous les futures ne sont pas résolus
x
tictoc::toc()</code></pre>
</div>
</div>
<div id="references" class="section level1 unnumbered">
<h1>References</h1>
<div id="refs" class="references">
<div id="ref-frogs">
<p>Escudié, Frédéric, Lucas Auer, Maria Bernard, Mahendra Mariadassou, Laurent Cauquil, Katia Vidal, Sarah Maman, Guillermina Hernandez-Raquet, Sylvie Combes, and Géraldine Pascal. 2017. “FROGS: Find, Rapidly, Otus with Galaxy Solution.” <em>Bioinformatics</em> 34 (8). Oxford University Press: 1287–94.</p>
</div>
</div>
</div>
