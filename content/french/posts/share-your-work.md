---
title: "Mettre en ligne son rapport d'analyse Rmd"
bibliography: biblio.bib
date: '2020-05-06'
categories: 
- Documentation
draft: no
language: french
level: intermediate
link-citations: yes
prerequites: 
- exemple-md
publishdate: '2020-05-06'
tags: 
- migale
thumbnail: rmd-share.jpg
authors: 
- Olivier Rué
traduction: no
type: post
w3codecolor: yes
---


Cette vignette propose différentes méthodes pour rendre accessible son rapport d'analyse sur le web. Ces méthodes sont basées sur les technologies du web en utilisant Rstudio. La génération des rapports et leur publication se fait ainsi dans le même environnement. Les principaux avantages sont de cette méthode sont :

- la facilité de mise en oeuvre avec <span class="tool">Rstudio</span>
- la traçabilité avec <span class="tool">git</span> et Gitlab
- la reproductibilité avec `R markdown`
- des rapports esthétiques et dynamiques

***

# Introduction


Dans le monde de la recherche, il est essentiel de présenter le travail effectué sous la forme de rapports, que ce soit en cours de projet pour présenter les avancées, les analyses déjà effectuées, ou en fin de projet pour synthétiser tout le travail effectué. Il est important que toutes les étapes y soient indiquées pour que le travail puisse être compris et reproduit.

Le cahier de laboratoire électronique se doit donc d'être facile d'utilisation et complet pour pouvoir y recenser:

- des lignes de commandes dans les languages utilisés pour l'analyse
- des tableaux
- des graphiques

mais aussi éventuellement:

- de la bibliographie
- de l'interactivité dans les tableaux et graphiques

<span style="color:red">R markdown est le language de prédilection pour répondre à toutes ces problématiques.</span> Son apprentissage est relativement simple. Ce language permet d'exécuter du code, d'afficher les résultats obtenus et d'ajouter du texte formaté. C'est le cahier de laboratoire intéractif par excellence. Une fois adopté, vous ne pourrez plus vous en passer !

<div class="row">
<div class="col-md-8">
Si le rapport est amené à évoluer, il faut également que la mise à jour du document soit facile et si possible que l'historique des modifications soit gardé. L'outil <span class="tool">git</span> permet d'avoir un historique des versions des différents fichiers centralisés dans un dépôt. En mode collaboratif, finis les échanges de fichiers par clés ou par pièces jointes ! Là encore, pour un usage basique, l'apprentissage est assez simple.
</div>
<div class="col-md-4">
<img src="../../final-doc.png" style="max-width:30%;" class="img-responsive">
</div>
</div>

<br>
<span class="tool">Rstudio</span> est l'environnement de développement idéal pour gérer vos rapports d'analyse. Vous verrez que dans une seule interface, vous aurez la possibilité d'éditer votre rapport, le compiler et le publier sur un site web !

<p><br></p>
<div class="alert comment">
Pour les allergiques à R, Rstudio et Rmarkdown peuvent être utilisés sans écrire une seule ligne de R !
</div>

***

# Le language R Markdown

L’extension rmarkdown permet de générer des documents de manière dynamique en mélangeant texte mis en forme et résultats produits par du code R (ou autres languages !). Les documents générés peuvent être au format HTML, PDF, Word, et bien d’autres. C’est donc un outil très pratique pour l’export, la communication et la diffusion de résultats d’analyse. Il est installé par défaut dans Rstudio.

Ce document a lui-même été généré à partir de fichiers R Markdown.

R Markdown offre une syntaxe simplifiée pour mettre en forme des documents contenant à la fois du texte, des instructions R et le résultat fourni par R lors de l’évaluation de ces instructions. En ce sens, il s’agit d’un outil permettant de produire des rapports d’analyse détaillés et commentés, plutôt que de simples scripts R incluant quelques commentaires.

Ce langage est basé sur Markdown. Il s’apprend très rapidement, ne nécessite rien d’autre qu’un éditeur texte.

Pour apprendre les bases du language R markdown, lisez <a href="">le tutoriel dédié</a>.

## Créer un nouveau document

Un document R Markdown est un simple fichier texte enregistré avec l’extension `.Rmd`.

Sous RStudio, on peut créer un nouveau document en allant dans le menu File puis en choisissant New file puis R Markdown…. Les boîtes de dialogue suivantes s’affichent et permettent de renseigner les métadonnées du nouveau fichier :

<div class="row">
<div class="col-md-6">
<img src="../../rmd-new.png" style="max-width:70%" alt="Rmd - New Document">
</div>
<div class="col-md-6">
<img src="../../rmd-new_document.png" style="max-width:70%" alt="Rmd - New Document">
</div>
</div>
<!--<p style="text-align:center">Créer un nouveau document Rmd</p>-->
<br>
On peut indiquer le titre, l’auteur du document ainsi que le format de sortie par défaut (il est possible de modifier facilement ses éléments par la suite). Un fichier comportant un contenu d’exemple s’affiche alors. Vous pouvez l’enregistrer où vous le souhaitez avec une extension .Rmd.

## Compiler un fichier Rmd en html

Le document Rmd se compile grâce au bouton `Knit`. On peut choisir le format de sortie. De base il est possible de choisir HTML, PDF ou Word.

<img src="../../rmd-knit_menu.png" alt="Rmd - New Document">
<br>

À gauche se trouve un exemple basique de fichier R markdown et à droite le rendu au format html :

<div class="row">
<div class="col-md-6">
<img src="../../premier-rmd-txt.png">
</div>

<div class="col-md-6">
<img src="../../premier-rmd.png">
</div>
</div>

Il est également possible de générer des fichiers au format PDF et docx :

<br><br>
<div class="row">
<div class="col-md-6">
<img src="../../premier-rmd-pdf.png">
</div>
<div class="col-md-6">
<img src="../../premier-rmd-docx.png">
</div>
</div>


# Publier son rapport avec Rpubs

<img src="../../rpubs-icon.png"></img>
Rstudio donne la possibilité, très facilement, de mettre à disposition le document compilé sur le site rpubs.com.



<div class="row">

<div class="col-md-9">
Pour créer un compte sur Rpubs, il suffit de suivre ces étapes :

- Se rendre à l'adresse https://rpubs.com/
- Cliquer sur le bouton Register en haut à droite
- Renseigner les champs requis
</div>
<div class="col-md-3">
<img src="../../rmd-rpubs.png" class="img-responsive" style="max-width:50%" alt="Rmd - New Document">
</div>
</div>

Ensuite, une fois le rapport généré, un bouton est accessible dans l'interface de Rstudio pour publier le rapport. Il suffit ensuite de cliquer sur le bouton Publier pour que le rapport web soit déployé sur Rpubs.

<div class="row">
<div class="col-md-2">
<img src="../../rmd-publish.png" alt="Rmd - New Document">
</div>
<div class="col-md-4">
<img src="../../rmd-publish_to_rpubs.png" style="max-width:70%" alt="Rmd - New Document">
</div>
<div class="col-md-6">
<img src="../../rpubs-details.png" style="max-width:90%" alt="Rmd - New Document">
</div>
</div>

Le document que j'ai nommé *test* est alors accessible à tous à cette adresse : https://rpubs.com/orue/test.
Pour le mettre à jour, rien de plus simple, il suffit de modifier le document, de le regénérer avec Knitr et de le publier à nouveau.


# Publier son rapport avec Gitlab

<img src="../../gitlab-icon.png" style="max-width:20%"></img>

Gitlab est une plateforme de développement à la fois open source et collaborative, qui se base sur <span class="tool">git</span>. Il tend à remplacer Github depuis son rachat par Microsoft en 2018.

À l'échelle d'INRAE, le département MathNUM (anciennement MIA) utilise Gitlab comme <a href="https://informatique-mia.inra.fr/Forgemia"forge logicielle</a> pour y stocker des projets menés par les membres des équipes du département. Elle est ouverte à tous les collaborateurs, même hors INRAE.

Utiliser un logiciel de gestion de versions (Gitlab, Github ou un autre) est toujours une bonne pratique. Les raisons suivantes vous convaincront sûrement :

- documents centralisés (on évite de tout perdre si l'ordinateur lâche ou si on fait une fausse manipulation)
- docuement versionnés (on garde une trace de toutes les modifications)
- facile et pratique pour le travail collaboratif

Il est indispensable de bien comprendre la philosophie et le vocabulaire spécifique de Git pour utiliser Gitlab au quotidien. Dans le cas d'une utilisation basique, mono-utilisateur, vous verrez que seules quelques actions sont essentielles :

- `commit`
- `push`

Enfin, ces outils proposent souvent une fonctionnalité pour pouvoir y déployer et rendre accessible sur le web des fichiers HTML. Elle s'appelle *Pages*.

## Gitlab Pages

<img src="../../gitlab-pages-icon.png" style="max-width:10%"></img>

Gitlab Pages est un service pour générer des sites web statistique à partir de votre repository Git via les outils d'intégration continue de Gitlab. Pour les utilisateurs de la forge du département MathNum, l'url d'accès au site web sera en https://username.pages.mia.inra.fr/nom_projet ou https://groupe.pages.mia.inra.fr/nom_projet.

### Création d'un projet

Pour créer un projet sur Gitlab, il suffit de cliquer sur le bouton <code>New project</code> et de rentrer les informations demandées. Je choisis ici de créer un projet nommé rmd-reports dans le projet `Gitlab pages`.

<img src="../../gitlab-rmd-reports-project.png" style="max-width:50%"></img>

Ensuite il faut cloner le dépôt depuis l'adresse du projet, en local (ordinateur personnel par exemple). Cette adresse est indiquée lorsqu'on clique sur le bouton `Clone`.

Les commandes suivantes sont à exécuter dans un terminal.

```bash
git clone git@forgemia.inra.fr:CATI-Boom/gitlab-pages/rmd-reports.git
cd rmd-reports
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

Un fichier README (vide) a été ajouté au dépôt puis déployé grâce à la commande push. Ce fichier est visible depuis l'interface Gitlab :

<img src="../../rmd-rmd-reports-add-readme.png" style="max-width:50%"></img>

Il est possible de modifier les fichiers depuis l'interface. Par exemple, pour modifier le fichier README il suffit de cliquer dessus et de clique sur le bouton Edit.

### Ajout du fichier .gitlab-ci.yml

Ce fichier servira à déployer le site web à partir de ce dépôt. Voici un exemple basique qui va nous servir pour ce premier exemple:


{{< highlight yaml >}}
# This file is a template, and might need editing before it works on your project.
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master
{{< / highlight >}}

<img src="../../rmd-rmd-reports-add-gitlab-ci.png" style="max-width:50%"></img>

De façon très simplifiée, ces lignes permettent d'activer le mode Pages et de déployer le contenu à chaque modification du dépôt. Dès qu'une modification est apportée, les commandes qui suivent l'instruction script sont exécutées, à savoir ici la création d'un répertoire .public et la copie de tout le dépôt dans ce répertoire. Enfin ce répertoire .public est renommé en public. C'est le contenu de ce répertoire qui sera accessible depuis la page gitlab.

### Organisation du dépôt

Je crée un fichier Rmd que je nomme first.Rmd. Je le compile ensuite avec le bouton Knit pour générer le fichier html.

Le bouton Git indique que les fichiers n'ont pas été ajoutés au dépôt. Il faut tous les sélectionner et les cocher.

<img src="../../rmd-rmd-reports-add-files.png" style="max-width:50%"></img>

Ensuite il faut appuyer sur le bouton Commit et indiquer un message expliquant les modifications effectuées puis sur le bouton Push. Les fichiers sont ajoutés au dépôt et visibles sur l'interface Gitlab.

L'adresse de déploiement de la page peut être retrouvée sur la forge dans Settings / Pages.

<img src="../../rmd-rmd-reports-page.png" style="width:80%"></img>

Ce lien <a href="https://cati-boom.pages.mia.inra.fr/gitlab-pages/rmd-reports/first.html">https://cati-boom.pages.mia.inra.fr/gitlab-pages/rmd-reports/first.html</a> est donc la nouvelle adresse du rapport !

On va faire le même travail avec un deuxième rapport appelé second.Rmd. Je colle un contenu trouvé sur le web. Je le compile et déploie les fichiers avec git.

Il est possible de créer un fichier index.html qui permet de lister les rapports que vous souhaitez mettre à disposition. Bien sûr, pour ce faire, on crée le fichier .Rmd que l'on compile ensuite. Pour faire des liens en Rmd, il existe une syntaxe particulière :

<img src="../../rmd-rmd-reports-index.png"></img>

Désormais, l'adresse <a href="https://cati-boom.pages.mia.inra.fr/gitlab-pages/rmd-reports/">https://cati-boom.pages.mia.inra.fr/gitlab-pages/rmd-reports/</a> vous permet d'accéder à tous les rapports que vous souhaitez mettre à disposition.

# Distill

<img src="../../distill-icon.png" style="max-width:100%"></img>

Je vous propose maintenant maintenant d'utiliser un package R : *distill* pour gérer vos rapports d'analyse.

Distill est un package R qui permet de gérer une collection de rapports sous la forme d'un blog. La documentation est accessible ici : <a href="https://rstudio.github.io/distill/">https://rstudio.github.io/distill/</a>. Il est très pratique et facile d'utilisation.

## Installation

L'installation est on ne peut plus simple. Il suffit d'installer le package distill dans la console de Rstudio. La version minimale de Rstudio pour pouvoir installer distill est RStudio v1.2.718. Pour installer une version plus récente de Rstudio, suivez ce lien : <a href="https://www.rstudio.com/products/rstudio/download/">https://www.rstudio.com/products/rstudio/download/</a>. Voici deux exemples de réalisation de blogs avec ce package :

<div class="row">
<div class="col-md-6">
<a href="https://blogs.rstudio.com/ai/"><img src="../../rmd-distill-example.png" class="img-responsive" style="max-width:30%"></img></a>
</div>
<div class="col-md-6">
<a href="https://tutorials.migale.inra.fr"><img src="../../rmd-distill-migale-tutorials.png" class="img-responsive" style="max-width:30%"></img></a>
</div>
</div>

<br>
Pour installer le package distill, il suffit de taper cette instruction dans la console de Rstudio :

```r
install.packages("distill")
```

## Créer un projet Gitlab pour héberger le blog

Comme vu précédemment, j'ai créé un nouveau projet nommé `distill` ici : https://forgemia.inra.fr/CATI-Boom/gitlab-pages/distill. Je le laisse vide.

Je rapatrie ensuite le dépôt en local :

```bash
git clone git@forgemia.inra.fr:CATI-Boom/gitlab-pages/distill.git
Clonage dans 'distill'...
warning: Vous semblez avoir cloné un dépôt vide.
```

Le warning est tout à fait normal. Ensuite on passe dans Rstudio

Dans la console Rstudio :

```r
distill::create_blog(dir = "~/GIT/distill", title="My blog")
```

Puis ouvrir le fichier `distill.Rproj`

Une arborsecence de fichiers a été créée dans le répertoire distill. C'est le squelette de notre blog distill.

<img src="../../distill_arborescence_raw.png" alt="Rmd - New Document">

Pour visualiser le blog, il faut cliquer sur le bouton `Build website`. Le blog ne contient qu'un article. Il est stocké dans le répertoire ~/GIT/distill/_posts/welcome. Il est écrit en R markdown et le fichier html se trouve à côté.



Commit and push

Ajouter contenu :
library(distill)
create_post("My first report", date_prefix = F)
Knitr puis Build Website




# References