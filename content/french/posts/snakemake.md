---
title: Snakemake
language: french
traduction: false
authors: 
- Cédric Midoux
categories: 
- Documentation
tags:
- migale
level: facile
prerequites:
- migale
date: 2020-05-11
publishdate: 2020-05-11
draft: false
type: post
thumbnail: snakemake_preview.png
w3codecolor: true
toc_depth: 4
#comments: true
#bibliography: biblio.bib
#nocite: '@*'
---

# Snakemake  

![](https://raw.githubusercontent.com/snakemake/snakemake/6d3aa424cfffaf33f36ca4b6da870ce786ddcb30/images/logo.svg)

`snakemake` est un gestionnaire de workflow basé sur le langage Python, développé par Johannes Köster depuis 2011 ([ref](https://doi.org/10.1093/bioinformatics/bts480)). Il permet d'ordonner et d'enchainer différents outils pour créer des analyses reproductibles, évolutives et portables. Ceci en fait donc un outil idéal pour réaliser vos pipeline d'analyses.

![](snakemake-schema.png)

# Principes de bases

Les workflows Snakemake sont des scripts Python compléter par des **règles** définies avec le mot-clé `rule`. Les règles décrivent comment créer des fichiers de sortie (`output`) à partir de fichiers d'entrée (`input`). Les règles peuvent être vu comme les étapes ou - briques élémentaire - du workflow.

* Pour chaque fichier intermédiaire, une règle définie comment il est créé et à partir de quels fichiers d'entrée.
* Snakemake détermine l'enchaînement des règles en faisant correspondre les **noms des fichiers**.
* Au sein d'une règle, il est possible d'utiliser des jokers (**`wildcards`**) reliant les noms de fichier d'entrée et de sortie de la règle.
* Les règles peuvent utiliser des commandes **shell**, du **code Python** simple ou des **scripts externes** en Python ou R par exemple. Ces règles permettent de créer des fichiers de sortie à partir de fichiers d'entrée.
* Les règles sont enchainés **automatiquement**, sans intervention manuelle. 
* Lorsque cela est possible, les règles sont exécutées **parallèlement**. 
* Pour facilité la **portabilité**, pour chaque règle, il est possible de paramétrer l'utilisation en CPU et en mémoire. Il est également possible de définir interaction avec un gestionnaire de *jobs* tel que SGE. Lorsque l'infrastructure le permet, snakemake peut déployer des environnements logiciels en utilisant Conda ou Singularity.

# Découverte grâce à un exemple

> Cet exemple s'inspire de celui de la documentation officielle. Vous pouvez la retrouver sur le [site de Snakemake](https://snakemake.readthedocs.io/en/stable/tutorial/basics.html) en anglais. 
>
> Pour reproduire cet exemple, après vous être connecté à migale (voir [tutoriel sur l'utilisation des ressources]()), créez un dossier dédié, téléchargez y  les [données du tutoriels](https://github.com/snakemake/snakemake-tutorial-data), placez vous sur un nœud interactif grâce à `qlogin` et activez l'environnement conda de Snakemake. Nous reviendrons sur ce point dans le chapitre suivant.
>
> ```shell
> [user@local ~]$ ssh stage01@migale.jouy.inrae.fr
> [stage01@migale ~]$ mkdir snakemake-tutorial
> [stage01@migale ~]$ cd snakemake-tutorial
> [stage01@migale snakemake-tutorial]$ wget https://github.com/snakemake/snakemake-tutorial-data/archive/v5.4.5.tar.gz
> [stage01@migale ~]$ qlogin
> Your job 2457067 ("QLOGIN") has been submitted
> [stage01@n64 ~]$ cd snakemake-tutorial
> [stage01@n64 snakemake-tutorial]$ tar -xf v5.4.5.tar.gz --strip 1
> [stage01@n64 snakemake-tutorial]$ conda activate snakemake-5.7.4
> (snakemake-5.7.4) [stage01@n64 snakemake-tutorial]$  tree data/
> data/
> ├── genome.fa
> ├── genome.fa.amb
> ├── genome.fa.ann
> ├── genome.fa.bwt
> ├── genome.fa.fai
> ├── genome.fa.pac
> ├── genome.fa.sa
> └── samples
>     ├── A.fastq
>     ├── B.fastq
>     └── C.fastq
> 
> 1 directory, 10 files
> ```

Cet exemple s'intéresse à la recherche de variants génomiques  "A", "B" et "C" par rapport à une souche de référence connue `genome.fa`. Ce tutoriel ne nécessite aucune connaissance particulière en génomique.

> > ==créer environnement snakemake-tutorial==
> >
> >  https://github.com/snakemake/snakemake-tutorial-data/blob/master/environment.yaml
> >
> > ```sh
> > conda env create --name snakemake-tutorial --file environment.yaml
> > ```
> >
> > ==voir pour les data ?==

## Étape 1 : Mapping des reads

Notre première règle Snakemake permettra de produit un alignement des reads d'un échantillon contre un génome de référence à l'aide de l'outil `bwa mem`. Pour cela,  créons un nouveau fichier appelé **Snakefile** dans le dossier courant et définissons la règle suivante :

```python
rule bwa_map:
    input:
        "data/genome.fa",
        "data/samples/A.fastq"
    output:
        "mapped_reads/A.bam"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"
```

Cette règle s'appelle `bwa_map` et elle utilise trois directives :

* `input`: liste de fichiers nécessaires à la règle,
* `output`: liste de fichiers que crée la règle,
* `shell`: commande exécuté par la règle. Cette commande fait référence aux fichiers en entrée et sortie grâce à la notation entre accolades. Ici, la liste de fichiers d'entrée sera concaténée et séparée par un espace. Vous pouvez constater qu'il est possible d'utiliser des pipes et des flux de redirections au sein de la commande shell d'une règle.

Lorsqu'un workflow est exécuté, Snakemake tente de générer des fichiers cibles donnés. Les fichiers cibles peuvent être spécifiés en paramètre Snakemae en ligne de commande. 

```sh
(snakemake-5.7.4) [stage01@n64 snakemake-tutorial]$ snakemake -np mapped_reads/A.bam
```

En exécutant la commande ci-dessus dans le répertoire courant, nous demander à Snakemake de générer le fichier cible `mapped_reads/A.bam` à l'aide des règles contenues dans le `Snakefile` et en utilisant les fichiers disponibles. Le paramètre `-n` (ou `--dry-run`) permet de simuler l'exécution, sans l'exécuter réellement. Le paramètre `-p` (ou `--printshellcmds`)  permet d'afficher explicitement les commandes shell qui sont exécutées.

Pour générer les fichiers cibles, Snakemake applique les règles données dans le `Snakefile` de manière descendante. Chaque application d'une règle pour générer un ensemble de fichiers de sortie est appelée "*job*". Pour chaque fichier d'entrée d'un *job*, Snakemake détermine récursivement les règles qui doivent être appliquées pour le générer. Cela donne un graphe dirigé et non-cyclique de *jobs* (on parle de "DAG" pour *Directed Acyclic Graph*). 

Dans cette exemple, nous avons une seule règle et le DAG ne comprend qu'un seul *job*. Nous pouvons exécuter notre workflow :

```sh
(snakemake-5.7.4) [stage01@n64 snakemake-tutorial]$ snakemake mapped_reads/A.bam
```

Si le workflow est exécuté avec succès et que vous re-tentez d'exécuter la commande précédente, rien ne se passera car le fichier cible est déjà présent dans le dossier courant. Snakemake ne ré-exécute les *jobs* que si l'un des fichiers d'entrée est plus récent que l'un des fichiers de sortie ou si l'un des fichiers d'entrée sera mis à jour par un autre *job*.

## Étape 2 :  Généralisation de la règle de mapping des reads

La règle décrite lors de l'étape 1 ne fonctionne que pour l'échantillon A correspondant au fichier `data/samples/A.fastq`. Il est nécessaire de généraliser en utilisant des *wildcards*. Pour cela il suffi simplement de remplacer le A par le *wildcard* `{sampe}`.

```python
rule bwa_map:
    input:
        "data/genome.fa",
        "data/samples/{sample}.fastq"
    output:
        "mapped_reads/{sample}.bam"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"
```

Ainsi cette règle pourra être utiliser pour générer n'importe quel fichier cible de la forme `mapped_reads/{sample}.bam` en remplaçant le *wildcard* par une valeur appropriée. Cette valeur sera répercutée sur les fichiers d'entrés et détermine ainsi les fichiers nécessaire pour l'exécution du *job*. Il est possible d'utiliser plusieurs *wildcards* au sein d'une règle. Un *wildcard* donné n'a pas d'effet en dehors de la règle ou il est défini. Si besoin, il est possible de contraindre les *wildcards* grâce à des expressions régulières.

```sh
(snakemake-5.7.4) [stage01@n64 snakemake-tutorial]$ snakemake -np mapped_reads/B.bam
```

Avec cette commande, Snakemake détermine qu'il faut appliquer la règle `bwa_mpa`en remplaçant le *wildcard* `{sample}` par la valeur `B`. Il est possible de spécifier plusieurs fichiers cibles explicitement 

```sh
(snakemake-5.7.4) [stage01@n64 snakemake-tutorial]$ snakemake -np mapped_reads/A.bam mapped_reads/B.bam
```

ou bien cette astuce de bash qui développe les  combinaisons possible avec `{A,B}`:

```sh
(snakemake-5.7.4) [stage01@n64 snakemake-tutorial]$ snakemake -np mapped_reads/{A,B}.bam
```

## Étape 3 : Tri des alignements

Pour les étapes suivante de notre analyse, nous avons besoin de trier les fichiers d'alignement. Ceci peut être réalisé avec l'outil `samtools`. Nous ajoutons la règle suivante sous la règle `bwa_map` :

```python
rule samtools_sort:
    input:
        "mapped_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}"
```

Cette règle prendra le fichier BAM du répertoire `mapped_reads` en entrée et stockera une version triée dans le répertoire `sorted_reads`. Si un répertoire est absent (ici  `sorted_reads/`) il est automatiquement créé par Snakemake lors de l'exécution des *jobs*. Il est possible accéder aux valeurs des *wildcards* dans une commande shell, ici par exemple `{wildcards.sample}`. Remarquez également qu'il est possible de distribuer une commande shell sur plusieurs lignes. Veillez cependant à ne pas oublier d'espace entre deux arguments. 

```sh
(snakemake-5.7.4) [stage01@n64 snakemake-tutorial]$ snakemake -np sorted_reads/B.bam
```

L'exécution de cette commande nous permet de visualiser l'enchainement des règle :  d'abord la règle `bwa_map` puis la règle `samtools_sort` pour créer le cible `sorted_reads/B.bam`. Les dépendances sont résolues automatiquement en faisant correspondre les noms des fichiers.

## Étape 4 : Index et visualisation du graphe DAG des *jobs*

Ensuite, nous devons à nouveau utiliser les `samtools` pour indexer les alignements triés. Pour cela, nous ajoutons la règle suivante sous les précédentes :

```python
rule samtools_index:
    input:
        "sorted_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam.bai"
    shell:
        "samtools index {input}"
```

A ce stade, nous avons trois règles qui enchainent. C'est un bon moment pour examiner de plus près la visualisation des graphes DAG des *jobs*. 

```sh
(snakemake-5.7.4) [stage01@n64 snakemake-tutorial]$ snakemake --dag sorted_reads/{A,B}.bam.bai | dot -Tsvg > dag.svg
```

Pour chaque fichier cible, Snakemake crée un DAG qui peut être visualisé en SVG grâce à la commande `dot`.

![../_images/dag_index.png](https://snakemake.readthedocs.io/en/stable/_images/dag_index.png)

Le graphe contient un nœud pour chaque *job* et les dépendances sont représentés par les arêtes. Pour les règles avec des *wildcards*, les valeurs correspondantes sont précisées. Si certains *jobs* n'ont pas besoin d'être exécutés, le nœud correspondant est affiché en pointillé.

## Étape 5 : Identification des variantes génomiques

L'étape suivante consiste à agréer les reads mappés de chaque échantillon et à rechercher les variants génomiques. Pour cela nous utiliserons les outils `samtools` et `bcftools`. Nous utiliserons également le fonction `expand` fournie par Snakemake qui facilite la sélection et l'agrégation de plusieurs fichiers. `expand` renvoi la liste de toutes les combinaisons suivant le pattern fourni. 

Par exemple `expand("sorted_reads/{sample}.bam", sample=SAMPLES)` avec `SAMPLE = ["A", "B"]` renverra `["sorted_reads/A.bam", "sorted_reads/B.bam"]`. 

Ceci est particulièrement efficace lorsqu'il y a plusieurs pattern à remplacer. Par exemple, `expand("sorted_reads/{sample}.{replicate}.bam", sample = ["A", "B"], replicate = [0, 1])` permet d'exprimer la liste des quatre combinaisons, c'est à dire : `["sorted_reads/A.0.bam", "sorted_reads/A.1.bam", "sorted_reads/B.0.bam", "sorted_reads/B.1.bam"]`.

Revenons à notre workflow d'analyse, tout d'abord, au début de notre Snakefile, définissons la liste des échantillons. N'oubliez pas que les Snakefiles sont des code Python. On peut donc y utiliser les variables Python ou tous autres possibilités qu'offre Python. Il serait possible d'utiliser un fichier de configuration externe, mais pour l'instant, contentons nous de définir la liste suivante :

```python
SAMPLE = ["A", "B"]
```

Pour la nouvelle règle, ajoutons sous les précédentes :

```python
rule bcftools_call:
    input:
        fa="data/genome.fa",
        bam=expand("sorted_reads/{sample}.bam", sample=SAMPLES),
        bai=expand("sorted_reads/{sample}.bam.bai", sample=SAMPLES)
    output:
        "calls/all.vcf"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"
```

Notez que les fichiers d'entrés sont nommés pour identifiés spécifiquement, par exemple avec `{input.fa}` dans la commande shell. Par ailleurs, vous remarquerez que les listes de fichiers d'entrée ou de sortie peuvent contenir des instructions Python, à condition que celles-ci renvoient une chaîne de caractères ou une liste de chaînes de caractères. Ici nous utilisons la fonction `expand` que nous avons abordé précédemment, pour lister et agréger les fichiers bam d'alignement de reads triés.

Si vous générez le graphe DAG ayant pour fichier cible `calls/all.vcf` vous devriez obtenir un graphe ressemblant au suivant :

![../_images/dag_call.png](https://snakemake.readthedocs.io/en/stable/_images/dag_call.png)

 ## Étape 6 : Utilisation de scripts personnels

Lors de la construction d'un workflow, en plus de l'utilisation d'outils, il est courant de faire appel à des scripts personnels. Bien qu'il soit possible d'écrire quelques lignes en Python au sein d'une règle, lorsque ces scripts sont assez long, il est généralement plus facile de le faire dans un fichier séparé. Pour faire appel à ce script dans une règle, il faudra alors utiliser la directive `script`. Ajoutez la règles suivant qui utilise un script externe personnel.

```python
rule plot_quals:
    input:
        "calls/all.vcf"
    output:
        "plots/quals.svg"
    script:
        "scripts/plot-quals.py"
```

Pour notre workflow, cette règle permet de générer un histogramme des scores de qualité qui ont été attribués aux variants défini dans le fichier d'entré `plots/quals.svg`. Pour cela, on utilise le script Python `scripts/plot-quals.py`. Lors que l'exécution de ce script, tous les paramètres et variables de la règles (`input`, `output`, `wildcards`, etc ...) sont accessibles à travers d'un objet global nommé `snakemake`. Créez le fichier `scripts/plot-quals.py`, avec le contenu suivant :

```python
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from pysam import VariantFile

quals = [record.qual for record in VariantFile(snakemake.input[0])]
plt.hist(quals)

plt.savefig(snakemake.output[0])
```

De la même manière, il est possible de faire appel à un script `R`.

## Étape 7 : Ajouter une règle cible

Jusqu'à présent, nous avons toujours exécuté le workflow en spécifiant un ou plusieurs fichiers cibles comme paramètre de `snakemake` lors de l'exécution. Il est également possible d'indiquer le nom d'une règle (n'utilisant pas *wildcard*) comme cible. Ainsi, il est possible de définir des règles cibles prenant en fichiers d'entrés couramment utilisé ensemble. De plus, si aucune cible est précisé, Snakemake utilisera par défaut la première règle du Snakefile comme cible. Il est donc judicieux d'avoir comme première règle du Snakefile, une règle générale qui contient tous les fichiers cibles généralement souhaités comme fichiers d'entrée. Par habitude, cette règle se nome `all`. Ajoutons cette règle au début du Snakefile.

```python
rule all:
    input:
        "plots/quals.svg"
```

Désormais, il n'est plus nécessaire de spécifier une cible lors de exécution de Snakemake.

```sh
(snakemake-5.7.4) [stage01@n64 snakemake-tutorial]$ snakemake -n
```

## Résumé <!--collapsed ?-->

Au final, le workflow ressemble à ceci :

```python
SAMPLES = ["A", "B"]


rule all:
    input:
        "plots/quals.svg"


rule bwa_map:
    input:
        "data/genome.fa",
        "data/samples/{sample}.fastq"
    output:
        "mapped_reads/{sample}.bam"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"


rule samtools_sort:
    input:
        "mapped_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}"


rule samtools_index:
    input:
        "sorted_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam.bai"
    shell:
        "samtools index {input}"


rule bcftools_call:
    input:
        fa="data/genome.fa",
        bam=expand("sorted_reads/{sample}.bam", sample=SAMPLES),
        bai=expand("sorted_reads/{sample}.bam.bai", sample=SAMPLES)
    output:
        "calls/all.vcf"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"


rule plot_quals:
    input:
        "calls/all.vcf"
    output:
        "plots/quals.svg"
    script:
        "scripts/plot-quals.py"
```

# Compléments

## Paramètres de Snakemake utile

### Lors de exécution

Voici quelques options facilitant l'utilisation de Snakemake. Vous pouvez retrouver l'aide complète avec `snakemake --help`

* `--dryrun` ou `-n` : les *jobs* ne sont pas exécutés. Permet de tester leur enchainement.

* `--snakefile` ou `-s` : si votre Snakefile à un nom autre, il est nécessaire de spécifié son chemin relatif.

* `--configfile` : spécifie un fichier de config au format YAML ou JSON. Les variables qui y sont défini peuvent être appelées avec la syntaxe `config["variable"]`. Par exemple dans le workflow du chapitre précédent, on peut définir un fichier `config.yaml` contenant l'information suivante :

  ```yaml
  SAMPLE: ["A", "B"]
  ```

  Puis on peut utiliser `config["SAMPLE"]`. Ceci permet de séparer les règles des paramètres de configuration. Il est également possible d'indiquer la position avec `configfile: "config.yaml"` en en-tête du Snakefile.

* `--keep-going` : par défaut, si un *job* échoue, aucun *job* suivant n'est lancé. En rajoutant cette option, vous autorisez Snakemake à exécuter les *jobs* disposant de tous leurs fichiers d'entrées (qui ne sont pas issus d'un *job* ayant échoué) même si un *job* parallèle a échoué

* `--forceall` ou `-F` : permet de forcer la ré-exécution des règles, sans tenir compte des sorties déjà créée.

* `--printshellcmds` ou `-p` : imprime explicitement les commandes shell exécutées dans les logs.

* `verbose `

>  Comme pour les autres outils, l'[aide complète](https://snakemake.readthedocs.io/en/stable/executing/cli.html#all-options) est disponible avec `snakemake --help`

### Rapport & visualisation

D'autre part, ces options permettent de visualiser et d'explorer son workflow : 

* `--report`  : génère un rapport exécution succin au format HTLM à postériori. ==Voici par exemple le rapport généré par l'exemple du chapitre précédent==. Il est possible d'afficher des résultats, tel que  des plots ou des pages HTML, directement dans unrapport. Pour toute utilisation avancé des rapport, référez vous à la [documentation spécifique](https://snakemake.readthedocs.io/en/stable/snakefiles/reporting.html). 
* `--dag`, `--rulegraph` et `--d3dag` : construit le graphe des *jobs* ou des règles qui pourra être visualisé avec `| dot -Tsvg > dag.svg`.

# Directives des règles

Lors de la présentation de l'exemple, nous avons vu l'utilisation de trois directives, il en existe d'autre. :

* `input`: liste de fichiers nécessaires à la règle. 

  * Cette liste peut être générée grâce à une fonction Python. Par exemple une fonction `lambda` qui pourra utiliser l'objet global `wildcards`. Voici un exemple illustratif (sans être forcément pertinent) de l'utilisation d'une *input function* avec lambda.

    ```python
    rule samtools_sort:
        input:
            lambda wildcards: "mapped_reads/A.bam" if wildcards.sample == "A" else "mapped_reads/B.bam"
            #"mapped_reads/{sample}.bam"
        output:
            "sorted_reads/{sample}.bam"
        shell:
            "samtools sort -T sorted_reads/{wildcards.sample} "
            "-O bam {input} > {output}"
    ```

    Ceci est particulièrement utile en synergie avec des variables définies dans le fichier de configuration.

  * Il est également possible d'utiliser une fonction Python définie avec `def` avant la règle l'utilisant. Dans ce cas, la fonction doit renvoyer une chaine de caractère correspondant à un seul fichier, une liste de chaine ou un dictionnaire. Pour les dictionnaire, la clé sera utilisé comme nom du fichier input et la valeur correspond à ce fichier. Il sera nécéssaire d'utiliser la fonction `unpack( )` lors de l'appel d'une *input function* renvoyant un dictionnaire. Voici un exemple illustratif (sans être forcément pertinent) de l'utilisation d'une *input function* renvoyant un dictionnaire.

    ```python
    def bwa_input(wildcards):
        if wildcards.sample == "A":
            return {"genome": "data/genome.fa", "reads": "data/samples/A.fastq"}
        elif wildcards.sample == "B":
            return {"genome": "data/genome.fa", "reads": "data/samples/B.fastq"}
        else:
            return {"genome": "data/genome.fa", "reads": "data/samples/{wildcards.sample}.fastq".format(wildcards=wildcards)}
        
    rule bwa_map:
        input:
            unpack(bwa_input)
        output:
            "mapped_reads/{sample}.bam"
        shell:
            "bwa mem {input.genome} {input.reads} | samtools view -Sb - > {output}"
    ```

* `output`: liste de fichiers créé par la règle. 

  * Pour rappel, l'utilisation de *wildcards* entre input et output est une notion majeure de Snakemake. 

  * Il est possible d'étiqueter un fichier output comme temporaire avec la syntaxe `temp( )`. Les fichiers temporaires sont supprimés par Snakemake lorsque tous les *jobs* ayant besoin de ce fichier ont été exécutés. Par exemple, pour l'exemple du chapitre précédent, cela donne :

    ```python
    temp("mapped_reads/{sample}.bam")
    ```

  * De même, il est possible d'étiqueter un fichier comme protégé avec `protected( )`. Les fichiers ainsi protégés, ne peuvent pas être écrasés ou  supprimé accidentellement. Cela donne :

    ```python
    protected("sorted_reads/{sample}.bam")
    ```
    
  * Pour certains outils ou bien par simplicité, il est possible d'avoir des répertoires, plutôt que des fichiers, comme résultats d'une règle. Cela s'exprime:

    ```python
    directory("sorted_reads/{sample}/")
    ```

    Attention, le contenue du répertoire indiqué est supprimé lors de exécution d'un *job*. Lorsque cela est possible il faut privilégier l'utilisation d'outputs standard.

* `shell` (peut être remplacé par`script` ou `run`) : la commande shell qui sera exécuté par la règle. Elle peut être remplacé par du code Python avec la directive `run` ou un script externe avec la directive `script`.

* `threads` : nombre de CPU à utiliser par la règle. Pour les informations générales sur les CPUs voir [tuto](). Cette directive n'est utile que lors de l'utilisation de Snakemake sur un cluster de calculs (voir chapitre suivant). Veillez à ne par utiliser directive lorsque vous utilisez Snakemake au cours d'une session interactive (`qlogin`) qui n'est pas multithreadée. Lorsque vous utilisez cette directive, vous pouvez utilisez la syntaxe de variable similaire aux *wildcards* : `{threads}` dans votre commande shell, notament pour indiquer à chaque outils combien de CPUs sont disponibles.

* `messages` : bref résumé de la règle imprimé dans la console lors sa soumission. Il est possible d'y exprimer des variables (par exemple `{wildcards.sample}`, `{input.genome}` ou `{threads}`)

* `priority` : priorité de la règle. Par défaut les règles ont une priorité de 0. Lorsque plusieurs *jobs* sont en attente simultanément, le *job* avec la priorité la plus élevée est soumis en premier.

* `log` : fichier où seront spécifié les logs de ce job. La variable `{log}` peut être utilisée à l'intérieur d'une commande shell pour indiquer explicitement à l'outil utilisé dans quel fichier écrire les logs.

* `params` : liste nommée de paramètres utilisables dans la commande shell. Permet de regrouper au même endroit tous les paramètres d'une règle. Les variables de la directives `params` peuvent être des fonction (`lambda` ou non), ceci de manipuler à volonté les autres variables.

>  Pour aller plus loin, la [documentation officielle](https://snakemake.readthedocs.io/en/stable/snakefiles/rules.html) dispose de nombreux détails et exemples.

# Usage spécifique sur le cluster migale

## Activer l'environnement Snakemake

Comme vous l'avez vu dans le tutoriel précédent, pour utiliser snakemake, il est nécessaire d'activer l'environnement conda associé

```sh
[stage01@n64 snakemake-tutorial]$ conda info --envs | grep snakemake
snakemake-4.7.0          /usr/local/genome/Anaconda2-5.1.0/envs/snakemake-4.7.0
snakemake-5.10.0         /usr/local/genome/Anaconda2-5.1.0/envs/snakemake-5.10.0
snakemake-5.4.5          /usr/local/genome/Anaconda2-5.1.0/envs/snakemake-5.4.5
snakemake-5.5.3          /usr/local/genome/Anaconda2-5.1.0/envs/snakemake-5.5.3
snakemake-5.7.4          /usr/local/genome/Anaconda2-5.1.0/envs/snakemake-5.7.4
[stage01@n64 snakemake-tutorial]$ conda activate snakemake-5.10.0
```

> Au jour de la rédaction de ce document, voici les versions de conda disponible. Nous utilisons dans cette exemple `snakemake-5.10.0`. 

==L'environnement `snakemake-tutorial` est dédié à l'exemple du début de ce tutoriel.== 

## Utiliser un outil avec un environnement conda

Comme indiqué dans le [tuto](), pour certains outils, il est nécessaire de charger un environnement conda pour utiliser l'outil associé. Pour charger un environnement au sein d'une règle, il est nécessaire d'initialiser l'utilisation de conda par Snakemake au niveau du Snakefile. Pour cela, il faut ajouter ces instructions comme premières lignes du Snakefile :

```
shell.executable("/bin/bash")
shell.prefix("conda_init; ")
```

> > ==ajouter `conda_init` dans le PATH avec `source` de ce fichier :==
> >
> > ```bash
> > # >>> conda initialize >>>
> > # !! Contents within this block are managed by 'conda init' !!
> > __conda_setup="$('/usr/local/genome/Anaconda2-5.1.0/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
> > if [ $? -eq 0 ]; then
> >     eval "$__conda_setup"
> > else
> >     if [ -f "/usr/local/genome/Anaconda2-5.1.0/etc/profile.d/conda.sh" ]; then
> >         . "/usr/local/genome/Anaconda2-5.1.0/etc/profile.d/conda.sh"
> >     else
> >         export PATH="/usr/local/genome/Anaconda2-5.1.0/bin:$PATH"
> >     fi
> > fi
> > unset __conda_setup
> > # <<< conda initialize <<<
> > ```

Avec cet ajout, il est désormais possible d'utiliser conda comme on pourrai le faire de manière interactive :

```python
rule conda_rule:
    input:
        ...
    output:
        ...
    shell:
        "conda activate <env> && "
        "<some cmd> && "
        "conda deactivate"
```

Pour rappel, la liste des environnements est disponible avec `conda info --envs`.

## Soumettre les *jobs* sur un cluster de calculs

Snakemake est capable interagir directement avec le gestionnaire de jobs du cluster Migale. Pour cela, il faut utiliser le paramètre `--cluster` qui prend comme argument la commande de soumission. Ce paramètre s'utilise conjointement avec `--jobs` qui défini le nombre maximum de *jobs* soumis simultanément. Conte-tenu, de architecture du serveur migale, il est nécessaire d'ajouter le paramètre `--latency-wait` avec un argument d'au moins 60.

```sh
$ snakemake --cluster "qsub -cwd -V" --jobs 10 --latency-wait 60
```

Il est possible d'utiliser les variables défini au sein des règles, en particulier pour `{threads}` :

```sh
$ snakemake --cluster "qsub -cwd -V -pe thread {threads}" --jobs 10 --latency-wait 60
```

Vous pouvez rajouter tous paramètres utilisables avec `qsub`. Si besoin, référez vous à [tuto]()

## Utilisez un fichier de configuration pour cluster

Comme `configfile` il est possible de définir un fichier de configuration dédié à l'exécution des *jobs* sur un cluster de calculs. Celui-ci vous permet de spécifier les paramètres de soumission de cluster en dehors du Snakefile. Le fichier de configuration de cluster est un fichier au format JSON ou YAML qui contient des objets qui correspondent aux noms des règles du Snakefile. Les variables cluster sont ensuite accessibles lorsque vous soumettez des *jobs* avec la syntaxe `{cluster.<var>}`.  Un objet `__default__` permet de définir des valeurs par défaut.

Pour l'exemple de chapitre précédent, on peut définir :

```json
{
	"__default__" :
	{
		"queue" : "short.q"
	},
	"samtools_index" :
	{
		"queue" : "long.q"
	}
}
```

```sh
$ snakemake --cluster "qsub -cwd -V -pe thread {threads} -q {cluster.queue}" --jobs 10 --latency-wait 60
```

Anisi les *jobs* par défaut seront exécutés sur la queue `short.q` et ceux provenant de la règle `samtools_index` le seront sur la queue `long.q`

## Exécuté un workflow sur un cluster de calcul

Comme tous calculs, la gestion de vos `jobs` est "attachée" à votre session. Pour un workflow durant plusieurs heures, voir plusieurs jours, il est nécessaire de l'exécuté en tant que job soumis au gestionnaire SGE. 

Voici un exemple de commande que l'on peut utiliser :

```sh
$ qsub -V -cwd -N workflow2020 -q long.q run_Snakemake.sh workflow_2020.smk
```

avec `run_Snakemake.sh` :

```sh
#!/bin/bash
source activate snakemake-5.10.0

mkdir -p .log/out/ .log/err/

snakemake \
--snakefile $1 \
--configfile config.json \ 
--cluster-config cluster.json \
--cluster "qsub -V -cwd -R y -N {rule} -o .log/out/ -e .log/err/ -q {cluster.queue} -pe thread {threads}" \
--keep-going \
--jobs 80 \
--latency-wait 60 \
--verbose \
--printshellcmds
```



---

**Vous avez a présent en main tous les éléments pour développer des workflow Snakemake dédié à vos analyses et les exécuter sur le cluster migale !**

