---
title: FROGS en ligne de commande
language: french
#traduction: true
authors: 
- Olivier Rué
categories: 
- Analyse
tags:
- metabarcoding
- FROGS
level: moyen
prerequites:
- unix
- cluster
date: 2020-04-08
publishdate: 2020-04-08
draft: true
type: post
thumbnail: frogs.png
w3codecolor: true
#comments: true
#bibliography: biblio.bib
#nocite: '@*'
---


Ce post a pour objectif de vous présenter l'organisation du serveur migale.

***

# Généralités

La plateforme Migale est une plateforme de bioinformatique proposant ...

## Obtenir un compte

L'obtention d'un compte est possible pour toute personne travaillant sur des données des sciences de la vie. Le formulaire est accessible sur le site de la plateforme : [https://migale.inrae.fr/ask-account](https://migale.inrae.fr/ask-account). Il vous est demandé une adresse mail académique.

<div class="alert comment">Cette adresse mail est essentielle car elle permet de vous contacter. Si vous deviez en changer, merci de nous le faire savoir sinon vous ne seriez plus au courant des informations importantes !</div>


# Connexion au serveur migale migale migale migale

Vous pouvez accéder à l’environnement Unix du serveur migale de façon différente suivant le système d'exploitation de votre ordinateur (Unix, MacOSX ou Windows).

## Unix

Sous Unix, vous pouvez accéder à l’environnement Unix du serveur migale via un terminal.

<pre class="bash"><code class="hljs lua">
ssh -X stage01@migale.jouy.inra.fr
</code>
</pre>

Vous devez ensuite indiquer votre mot de passe et taper sur Entrée.

S’affiche alors à l’écran un message d’acceuil ainsi que des instructions génériques et actualités, merci d’en tenir compte. Au début de chaque ligne s’affiche votre nom d’utilisateur et là où vous êtes connecté, séparé par un @.

<pre class="bash"><code class="hljs lua">
local:~$ ssh -X stage01@migale.jouy.inra.fr
Password:
Last login: Mon Feb 24 11:25:48 2020 from 138.102.22.243
  __  __ _           _     
 |  \/  (_)__ _ __ _| |___ 
 | |\/| | / _` / _` | / -_)
 |_|  |_|_\__, \__,_|_\___|
          |___/           
-----------------------
Bienvenue sur le serveur frontal de la  plateforme Migale,
Vous trouverez la liste des outils disponibles en ligne de commande sur le site de la plateforme :
https://migale.inra.fr/tools
Merci d utiliser le cluster pour vos calculs. 
Dans tous les cas, ne pas lancer de calcul long (supérieur à 1h) ou utilisant plus de deux processeurs sur ce serveur.
Pour toute question : help-migale@inra.fr
-----------------------
Welcome to the Migale facility front end server,
You will find the list of command line tools available on our website:
https://migale.inra.fr/tools
Please use the cluster for your calculations.
In any case, do not run a long jobs (more than 1 hour) or using more than two processors on this server.
For any questions: help-migale@inra.fr
-----------------------
[stage01@migale ~]$ 
</code>
</pre>



<details>
<summary>Démonstration en vidéo</summary>
<br>
<video width="80%" controls>
  <source src="../../../media/unix-connection.webm" type="video/webm">
</video>
</details>

## Windows

MobaXterm propose une application logicielle permettant d'utiliser sous Windows les commandes Unix/Linux. Ce logiciel est gratuit et très simple d'utilisation. Pour se connecter, vous serez amener à entrer l'adresse de l'hôte distant (remote host), en l'ocurrence `migale.jouy.inra.fr`, votre nom d'utilisateur (username), et ensuite votre mot de passe.

<details>
<summary>Démonstration en vidéo</summary>
<br>
<video width="80%" controls>
  <source src="../../../media/unix-connection.webm" type="video/webm">
</video>
</details>

# Les espaces de travail

La figure 1 représente l'organisation des comptes utilisateurs. Un répertoire `/projet` regroupe plusieurs répertoires représentant des groupes d'utilisateurs répartis selon leur unité d'affectation pour les agents INRAE ou un répertoire `extern` pour les autres. Dans chacun de ces répertoires se trouvent un répertoire `work` et un répertoire `save`. L'utilisateur `stage01` n'a naturellement accès qu'aux répertoires qui le concernent (en orange sur la figure). Lors de la connexion, vous êtes dirigés automatiquement dans le `home directory`. Il est placé dans le répertoire `save` associé à votre identifiant.


<div class="mermaid" style="text-align:center">
graph TD
    B["/projet"]
    B-->D["/projet/micalis"]
    B:::someclass-->C["/projet/maiage"]
    B-->E["/projet/..."]
    C:::someclass-->F["/projet/maiage/save"]
    C:::someclass-->G["/projet/maiage/work"]
    F:::someclass-->H["/projet/maiage/save/stage01"]
    F-->I["/projet/maiage/save/..."]
    G:::someclass-->J["/projet/maiage/work/stage01"]
    G-->K["/projet/maiage/work/..."]
    J:::someclass
    F:::someclass
    style H fill:#f96,stroke:#f66,stroke-width:8px
    classDef someclass fill:#f96;
    
</div>
<div style="text-align:center"><i>Figure 1 : Organisation des espaces utilisateurs</i></div>

## Save vs Work

Le répertoire `save` est un espace dédié aux fichiers de configuration de votre compte (voir `.bashrc` dans le tutoriel <a href="">unix</a>) et aux résultats que vous souhaitez conserver. Cet espace est doté d'un système de snapshots.

Le répertoire work est un espace dédié aux fichiers temporaires, qui n'ont pas vocation à être conservés sur une longue période. C'est un espace de travail au quotidien.

## Volumétrie

Les espaces `work` et `save` ont des volumétries distinctes. Actuellement, les quotas sont partagés entre les membres d'un même groupe.

## Espaces projets

Certains espaces sont dédiés à un projet. Par exemple, l'espace projet _redlosses_ est lié au groupe unix `redlosses`. Chaque membre du groupe `redlosses` peut donc écrire dans les répertoires `work` et `save` associés.

<div class="mermaid" style="text-align:center">
graph TD
    B["/projet"]
    B-->D["/projet/proteore"]
    B:::someclass-->C["/projet/redlosses"]
    B-->E["/projet/..."]
    C:::someclass-->F["/projet/redlosses/save"]
    C:::someclass-->G["/projet/redlosses/work"]
    F:::someclass
    G:::someclass
    classDef someclass fill:#f96;
    
</div>
<div style="text-align:center"><i>Figure 2 : Organisation des espaces projets</i></div>



<pre class="bash"><code class="hljs lua">
[orue@migale ~]$ ls -ltrah /projet/redlosses/
total 12K
drwxr-xr-x   4 root root        30 21 avril  2017 .
drwxrwsr-x  15 root redlosses 4,0K 29 mai    2019 work
drwxrwsr-x  15 root redlosses 4,0K 29 mai    2019 save
drwxr-xr-x 117 root root      4,0K  2 avril 16:11 ..
</code></pre>

# Outils

De nombreux outils sont accessibles en ligne de commande. Vous pouvez retrouver la liste complète des outils ici : <a href="https://migale.inrae.fr/tools">https://migale.inrae.fr/tools</a>.
La majorité des outils sont installés sous l'environnement conda.

# Banques de données

De nombreuses banques de données sont accessibles dans un espace dédié nommé `/db`.



# Cluster de calcul


# Quizz

<div id="quiz">
  <div id="quiz-header">
    <p class="faded">Êtes-vous prêt à utiliser Migale ?</p>
  </div>
  <div id="quiz-start-screen">
    <p><a href="#" id="quiz-start-btn" class="quiz-button">Start</a></p>
  </div>
</div>

<script>
$('#quiz').quiz({
  //resultsScreen: '#results-screen',
  //counter: false,
  //homeButton: '#custom-home',
  counterFormat: 'Question %current of %total',
  questions: [
    {
      'q': 'Le serveur migale est accessible depuis Windows, MacOS et Unix',
      'options': [
        'Oui',
        'Non'
      ],
      'correctIndex': 0,
      'correctResponse': 'Bien sûr, avec le terminal pour Unix et MacOS, avec des outils comme putty ou MobaXterm pour Windows.',
      'incorrectResponse': 'Nous ne laissons personne de côté voyons !'
    },
    {
      'q': 'Si mon user est toto et mon groupe mygroup, quel est mon home directory ?',
      'options': [
        '/projet/mygroup/toto/work',
        '/projet/mygroup/save/toto',
        '/projet/mygroup/work/toto'
      ],
      'correctIndex': 1,
      'correctResponse': 'Bien ! Vous avez compris où se trouvent les home directories !',
      'incorrectResponse': 'Ah non, revisez le second chapitre, il est essentiel pour utiliser migale'
    },
    {
      'q': 'Je lance mes calculs directement sur le serveur migale',
      'options': [
        'Oui',
        'Non'
      ],
      'correctIndex': 1,
      'correctResponse': 'C\'est interdit ! Vous devez utiliser le cluster de calcul pour lancer vos analyses !',
      'incorrectResponse': 'Bon... votre compte est blacklisté...'
    }
  ]
});
</script>


